#include <stdio.h>
#include <stdbool.h>
#include "programa.h"
#include "queue.h"
#include <string.h>

#define QUANTUM 4 //Limitador do numero de ciclos que um processo pode estar em estado RUN
#define NEW 0     //Indentificador do estado NEW
#define WAIT 1    //Indentificador do estado WAIT
#define RUN 2     //Indentificador do estado RUN
#define BLOCK 3   //Indentificador do estado BLOCK
#define EXIT 4    //Indentificador do estado closef
#define MEM_SIZE 300
#define BESTFIT 1
#define NEXTFIT 0

int MEM[MEM_SIZE];
int memOccupied = MEM_SIZE - 1;
int memClean = MEM_SIZE - 1;
//Variaveis globais
int matrix[30][MEM_SIZE]; //Mapa dos processo (Copia do input)
int nextPid = 10;         //Variavel global para sabermos o numero do pid a dar a um novo processo.
int clockCycle = 0;       //Contador dos ciclos
int cont = 0;
int line = 0;

FILE *ficheiro;                       //Disco, onde guardamos os valores escritos pela intrução codificado pelo numero 8
int runCounter = 0;                   //Este contador é usado para garantir que um processo nao fica mais do que 4 ciclos no estado RUN
unsigned short escalonador = NEXTFIT; //escolher se vamos implementar um o algoritmo bestFit ou NextFit
int blocktoWait = 0;                  //Garante que não passamos do block para Wait e para o Run num unico ciclo
bool varToPrint = false;              //Variavel para saber quando imprimir um variavel
bool exitFlag = false;
bool blockFlag = false;

struct program newQueue[MEM_SIZE]; //Queue do NEW
int newQueueSize = 0;
struct program waitQueue[MEM_SIZE]; //Queue do WAIT
int waitQueueSize = 0;
struct program blockQueue[MEM_SIZE]; //Queue do BLOCK
int blockQueueSize = 0;
struct program inRun;       //Variavel que indica qual o processo que está em estado RUN (Ganhando acesso direto ao mesmo sem termos de estar a perder tempo a procurar)
struct program toPrint[30]; //Estrutura de armazenamento de todos programas que foram excecutados para fazer print dos estados em que cada um está
struct program exitProcess;
int xx = 0;
//Guarda o processo que chegou ao exit para que da proxima vez que chamamos o exit possamos alterar o estado do processo que la estava antes

//Modelo de 5 estados daqui para baixo

void runController();

struct program initProcess(struct program pt) //Função que inicializa um novo proceoss
{

    pt.ppcb.pid = nextPid;  //dá um PID ao novo processo
    nextPid = nextPid + 10; //Calcula o PID do proximo processo a ser criado
    pt.ppcb.pc = 10;        // Posição onde começa as instruções
    pt.ppcb.state = -1;
    for (int i = 0; i < 10; i++)
    {
        pt.mem[i] = 0;
    }
    for (int i = 11; i <= MEM_SIZE - 1; i++)
    {
        pt.mem[i] = -9;
    }
    return pt;
}

void print(FILE *fp, FILE *ss)
{
    if (clockCycle < 10)
    {
        fprintf(fp, "Instante: %d  ", clockCycle);
        fprintf(ss, "Instante: %d  ", clockCycle);
    }
    else
    {
        fprintf(fp, "Instante: %d ", clockCycle);
        fprintf(ss, "Instante: %d ", clockCycle);
        
    }

    for (int i = 0; i < cont; i++)
    {
        if (toPrint[i].ppcb.state == NEW)
        {
            fprintf(fp, "|  new  ");
            fprintf(ss, "|  new  ");
        }
        else if (toPrint[i].ppcb.state == WAIT)
        {
            fprintf(fp, "|  wait ");
            fprintf(ss, "|  wait ");
        }
        else if (toPrint[i].ppcb.state == RUN)
        {
            fprintf(fp, "|  run  ");
            fprintf(ss, "|  run  ");
        }
        else if (toPrint[i].ppcb.state == BLOCK)
        {
            fprintf(fp, "| block ");
            fprintf(ss, "| block ");
        }
        else if (toPrint[i].ppcb.state == EXIT)
        {
            fprintf(fp, "|  exit ");
            fprintf(ss, "|  exit ");
        }
        else
        {
            fprintf(fp, "|       ");
            fprintf(ss, "|       ");
        }
    }
    if (varToPrint)
    {
        fprintf(fp, "| %d", inRun.mem[xx]);
        fprintf(ss, "| %d", inRun.mem[xx]);
        varToPrint = false;
    }
    fprintf(fp, "| ");
    fprintf(ss, "| ");
    if (escalonador == NEXTFIT)
    {
        for (int i = MEM_SIZE - 1; i > 0; i--)
        {
            if (MEM[i] != -9)
            {
                fprintf(fp, "%d ", MEM[i]);
            }
        }
    }
    if (escalonador == BESTFIT)
    {
        for (int i = 0; i < MEM_SIZE; i++)
        {
            if (MEM[i] != -9)
            {
                fprintf(fp, "%d ", MEM[i]);
            }
        }
    }
    fprintf(fp, "\n");
    fprintf(ss, "\n");
}

int searchIndex(int pid) // encontra o indice do pid
{
    int first = 0;                   //Indetifica o indice do inicio do array
    int last = cont;                 //Indetifica o indice do fim do array
    int middle = (first + last) / 2; //Indetifica o indice do meio do array
    while (first <= last)
    {
        if (toPrint[middle].ppcb.pid < pid)
        {
            first = middle + 1;
        }
        else if (toPrint[middle].ppcb.pid == pid)
        {
            return middle;
        }
        else
        {
            last = middle - 1;
        }

        middle = (first + last) / 2;
    }
    return -1;
}

void close() //EXIT - Não metemos o nome da função para exit porque o complidador fava warnings por ser um nome parecido a uma função da biblioteca de c
{

    inRun.ppcb.state = EXIT; //Mete o estado do progrma para EXIT
    exitProcess = inRun;
    int indexToPrint;                                 //Indice que corresponde ao processo que estamos a trabalhar
    indexToPrint = searchIndex(exitProcess.ppcb.pid); //Procura o indice correspondente
    toPrint[indexToPrint].ppcb.state = EXIT;          //Altera o estado do processo no array

    for (int i = exitProcess.ppcb.memPos1; i < exitProcess.ppcb.memPos1 + exitProcess.size + 12; i++)
    {
        MEM[i] = -9;
        memClean++;
        //memOccupied++;
    }
    inRun.ppcb.pc = 11; // Posição onde começa as instruções
    inRun.ppcb.state = -1;

    for (int i = 0; i < 10; i++)
    {
        inRun.mem[i] = 0;
    }

    runCounter = 0;
    exitFlag = false;
    runController();
}

void block(int x)
{

    fseek(ficheiro, (x + inRun.ppcb.pid), SEEK_SET); //mete o cursor no sitio podemos querer escrever a variavel
    inRun.ppcb.state = BLOCK;
    fprintf(ficheiro, "%d", inRun.mem[x]); //Escreve a info x

    enqueue(blockQueue, inRun, blockQueueSize);
    blockQueueSize++;
    runCounter = 0;

    int indexToPrint;                           //Indice que corresponde ao processo que estamos a trabalhar
    indexToPrint = searchIndex(inRun.ppcb.pid); //Procura o indice correspondente
    toPrint[indexToPrint].ppcb.state = BLOCK;   //Altera o estado do processo no array toPrint
    initProcess(inRun);
    runController();
}

int fork(int varX, struct program process)
{
    struct program processoFilho;            //Novo process
    processoFilho.ppcb.pid = nextPid;        //dá um PID ao novo processo
    nextPid = nextPid + 10;                  //Calcula o PID do proximo processo a ser criado
    processoFilho.ppcb.pc = process.ppcb.pc; // Posição onde começa as instruções
    processoFilho.size = process.size;
    processoFilho.ppcb.state = -1;
    for (int i = 11; i < 300; i++) //copia as instruções
    {
        processoFilho.mem[i] = process.mem[i];
    }
    processoFilho.mem[varX] = 0; // mete a var x do filho a 0

    if (process.size < (MEM_SIZE - 1 - memOccupied) && !isFull(waitQueueSize)) // se houver espaço na memoria podemos criar o novo processo filho
    {
        int availableSize = 0;
        int contadorProcessMem = 0;

        //enviar o processo duplicado e para o array MEM

        if (escalonador == NEXTFIT) //nextfit
        {
            for (int i = MEM_SIZE - 1; i > 0; i--) //NEXTFIT
            {
                if (MEM[i] == -9)
                {
                    availableSize++;
                    if (availableSize >= processoFilho.size + 12)
                    {
                        processoFilho.ppcb.memPos1 = i + (availableSize - 1);
                        for (int j = i + (availableSize - 1); j < processoFilho.size + 12; j--)
                        {
                            if (processoFilho.mem[contadorProcessMem] != 9)
                            {
                                MEM[j] = processoFilho.mem[contadorProcessMem];
                                contadorProcessMem++;
                            }
                            else
                            {
                                availableSize = 0;
                                break;
                            }
                        }
                        availableSize = 0;
                        break;
                    }
                }
            }
        }
        else if (escalonador == BESTFIT) //BESTFIT
        {
            /* code */
        }

        //Enviar o processo duplicado para o wait
        enqueue(waitQueue, processoFilho, waitQueueSize);
        waitQueueSize++;
        processoFilho.ppcb.state = WAIT;
        toPrint[cont] = processoFilho;
        toPrint[cont].ppcb.state = WAIT; //Altera o estado do processo no array toPrint
        cont++;

        return processoFilho.ppcb.pid; //retorna para a variavel o pid do filho
    }
    else if (!isFull(newQueueSize))
    {
        enqueue(newQueue, processoFilho, newQueueSize);
        newQueueSize++;
        processoFilho.ppcb.state = NEW;
        toPrint[cont] = processoFilho;
        toPrint[cont].ppcb.state = NEW;
        cont++;
        return processoFilho.ppcb.pid; //retorna para a variavel o pid do filho
    }
    else
    {
        return -1; //Signifca que nao ha espaço // ALERTA DE AVERIGUAR ISTO MELHOR PORQUE ACHO QUE DEVEMOS EMITIR UM ERRO ESPESIFICO
    }
}

void toMem(struct program process)
{
    if (escalonador == NEXTFIT)
    {
        int j = 0;
        for (int i = process.ppcb.memPos1 + process.size + 11; i > process.ppcb.memPos1 + process.size; i--)
        {
            MEM[i] = process.mem[j];
            j++;
        }
    }
    if (escalonador == BESTFIT)
    {
        int j = 0;
        for (int i = process.ppcb.memPos1; i < process.ppcb.memPos1 + process.size + 11; i++)
        {
            MEM[i] = process.mem[j];
            j++;
        }
    }
}

void run() //Esta função Run vai agir como um descodificador das instrução
{

    int cod, x, y;
    inRun.ppcb.state = RUN;             //Aletramos o estado do proceso a ser executado para RUN
    cod = inRun.mem[inRun.ppcb.pc + 1]; //atribuimos os valores às variaveis
    x = inRun.mem[inRun.ppcb.pc + 2];
    y = inRun.mem[inRun.ppcb.pc + 3];

    inRun.ppcb.pc = inRun.ppcb.pc + 3; //altera o pc para o inicio da proxima instrução
    switch (cod)
    {
    case 0:
        inRun.mem[x] = inRun.mem[y];
        toMem(inRun);
        break;
    case 1:
        inRun.mem[x] = y;
        toMem(inRun);
        break;
    case 2:
        inRun.mem[x] = inRun.mem[x] + 1;
        toMem(inRun);
        break;
    case 3:
        inRun.mem[x] = inRun.mem[x] - 1;
        toMem(inRun);
        break;
    case 4:
        if (inRun.ppcb.pc + x < inRun.size + 9 && inRun.ppcb.pc + x > 11) //o pc pode apontar par auma variavel?
        {
            inRun.ppcb.pc = inRun.ppcb.pc - x;
        }
        else
        {
            printf("MEMORY ACCESS VIOLATION\n");
        }
        break;
    case 5:
        if (inRun.ppcb.pc + x < inRun.size + 9 && inRun.ppcb.pc + x > 11)
        {
            inRun.ppcb.pc = inRun.ppcb.pc + x;
        }
        else
        {
            printf("MEMORY ACCESS VIOLATION\n");
        }
        break;
    case 6:
        if (x == 0)
        {
            if (inRun.ppcb.pc + y < inRun.size + 9 && inRun.ppcb.pc + y > 11) //o pc pode apontar par auma variavel?
            {
                inRun.ppcb.pc = inRun.ppcb.pc + y;
            }
            else
            {
                printf("MEMORY ACCESS VIOLATION\n");
                exitFlag = true;
            }
        }
        break;
    case 7:
        inRun.mem[x] = fork(x, inRun);
        toMem(inRun);
        break;
    case 8:
        blockFlag = true;

        break;
    case 9:

        fseek(ficheiro, (x + inRun.ppcb.pid), SEEK_SET); //mete o cursor no sitio podemos querer escrever a variavel
        fread(&x, sizeof(int), 1, ficheiro);             //le o valor guardado em memoria para a variavel x
        toMem(inRun);
        break;
    case 10:
        varToPrint = true;
        xx = x;

        break;
    case 11:
        exitFlag = true;
        break;
    default:
        printf("MEMORY ACCESS VIOLATION\n");
        exitFlag = true;
        break;
    }
}

void runController()
{

    if (runCounter == 0 && !isEmpty(waitQueueSize)) // wait para o run, se counter =0 significa que run não tem nenhum processo
    {
        inRun = dequeue(waitQueue, waitQueueSize);

        int indexToPrint = searchIndex(inRun.ppcb.pid); //Procura o indice correspondente
        toPrint[indexToPrint].ppcb.state = RUN;         //Altera o estado do processo no array toPrint
        waitQueueSize--;
        runCounter++;
        run();
    }
    else if (runCounter >= QUANTUM && !isFull(waitQueueSize)) // Quantum break cycle - run para wait
    {
        enqueue(waitQueue, inRun, waitQueueSize);
        waitQueueSize++;
        int indexToPrint = searchIndex(inRun.ppcb.pid); //Procura o indice correspondente
        toPrint[indexToPrint].ppcb.state = WAIT;        //Altera o estado do processo no array toPrint
        runCounter = 0;
        runController();
    }
    else if (runCounter > 0 && runCounter < QUANTUM)
    {
        runCounter++;
        run();
    }
    else if (runCounter == 0 && isEmpty(waitQueueSize))
    {
        return;
    }
}

int blockController(int blockCounter)
{
    if (!isEmpty(blockQueueSize) && blockCounter < 3)
    {
        blockCounter++;
    }
    if (!isEmpty(blockQueueSize) && blockCounter == 3)
    {

        struct program moveToWait = dequeue(blockQueue, blockQueueSize);
        blockQueueSize--;
        blockCounter = 0;
        int indexToPrint;                                //Indice que corresponde ao processo que estamos a trabalhar
        indexToPrint = searchIndex(moveToWait.ppcb.pid); //Procura o indice correspondente
        toPrint[indexToPrint].ppcb.state = WAIT;         //Altera o estado do processo no array toPrint

        enqueue(waitQueue, moveToWait, waitQueueSize);
        waitQueueSize++;
    }

    return blockCounter;
}

void new (struct program process)
{
    if (process.ppcb.state == -1)
    {
        process.ppcb.state = NEW; //Altera o estado do processo
        enqueue(newQueue, process, newQueueSize);
        newQueueSize++;
        toPrint[cont] = process;
        cont++;
    }
}

void newToWait()
{

    while (!isEmpty(newQueueSize))
    {
        struct program inNew = peek(newQueue);
        if (escalonador == NEXTFIT)
        {

            if (runCounter == 0 && isEmpty(waitQueueSize))
            {
                struct program removedProgram;
                int indexToPrint; //Indice que corresponde ao processo que estamos a trabalhar

                removedProgram = dequeue(newQueue, newQueueSize);
                indexToPrint = searchIndex(removedProgram.ppcb.pid); //Procura o indice correspondente

                toPrint[indexToPrint].ppcb.state = RUN; //Altera o estado do processo no array toPrint
                newQueueSize--;
                removedProgram.ppcb.state = RUN;

                for (int i = 0; i < removedProgram.size + 11; i++)
                {
                    MEM[memOccupied] = removedProgram.mem[i];
                    memOccupied--;
                    memClean--;
                }
                removedProgram.ppcb.memPos1 = memOccupied;

                inRun = removedProgram;
                runCounter++;
                runController();
            }

            else if (!isFull(waitQueueSize) && inNew.size + 11 < memOccupied)
            {
                //printf("Tamanho Programa:%d Espaço Livre:%d\n", inNew.size, memOccupied);
                struct program removedProgram;
                int indexToPrint; //Indice que corresponde ao processo que estamos a trabalhar

                removedProgram = dequeue(newQueue, newQueueSize);
                indexToPrint = searchIndex(removedProgram.ppcb.pid); //Procura o indice correspondente

                toPrint[indexToPrint].ppcb.state = WAIT; //Altera o estado do processo no array toPrint
                newQueueSize--;

                removedProgram.ppcb.state = WAIT; //Altera o estado do processo

                for (int i = 0; i < removedProgram.size + 11; i++)
                {
                    MEM[memOccupied] = removedProgram.mem[i];
                    memOccupied--;
                    memClean--;
                }

                removedProgram.ppcb.memPos1 = memOccupied;

                enqueue(waitQueue, removedProgram, waitQueueSize);
                waitQueueSize++;
            }
            else
            {
                break;
            }
        }
        if (escalonador == BESTFIT)
        {
            bool flag = false;
            int emptySize = 0;
            int temp[4] = {0, 0, 0, 0};
            if (runCounter == 0 && isEmpty(waitQueueSize))
            {
                struct program removedProgram;
                int indexToPrint; //Indice que corresponde ao processo que estamos a trabalhar

                removedProgram = dequeue(newQueue, newQueueSize);
                indexToPrint = searchIndex(removedProgram.ppcb.pid); //Procura o indice correspondente

                toPrint[indexToPrint].ppcb.state = RUN; //Altera o estado do processo no array toPrint
                newQueueSize--;
                removedProgram.ppcb.state = RUN;

                for (int i = 0; i < removedProgram.size + 11; i++)
                {
                    MEM[i] = removedProgram.mem[i];
                }
                removedProgram.ppcb.memPos1 = 0;

                inRun = removedProgram;
                runCounter++;
                runController();
            }

            else if (!isFull(waitQueueSize) && inNew.size + 11 < memOccupied)
            {
                struct program removedProgram;
                int indexToPrint; //Indice que corresponde ao processo que estamos a trabalhar

                removedProgram = dequeue(newQueue, newQueueSize);
                indexToPrint = searchIndex(removedProgram.ppcb.pid); //Procura o indice correspondente

                toPrint[indexToPrint].ppcb.state = WAIT; //Altera o estado do processo no array toPrint
                newQueueSize--;
                removedProgram.ppcb.state = WAIT;

                for (int i = 0; i < 300; i++)
                {
                    if (MEM[i] == -9 && flag == false)
                    {
                        flag = true;
                        emptySize++;
                        if (temp[0] == 0)
                        {
                            temp[0] = i;
                        }
                        else
                        {
                            temp[2] = i;
                        }
                    }
                    else if ((MEM[i] == -9 && flag == true) || (MEM[i] == -9 && flag == true && i == MEM_SIZE - 2))
                    {
                        emptySize++;
                    }
                    else if (MEM[i] != -9 && flag == true)
                    {
                        flag = false;
                        if (temp[0] != 0)
                        {
                            temp[1] = emptySize;
                        }
                        else
                        {
                            temp[3] = emptySize;
                        }
                    }
                    if (temp[1] != 0 && temp[3] != 0)
                    {
                        if (temp[1] - inNew.size + 11 == 0)
                        {
                            break;
                        }
                        else if (temp[3] - inNew.size + 11 == 0)
                        {
                            temp[0] = temp[2];
                            temp[1] = temp[3];
                            break;
                        }
                        else if ((temp[1] - inNew.size + 11 > 0) && (temp[1] - inNew.size + 11) <= (temp[3] - inNew.size + 11))
                        {
                            temp[2] = 0;
                            temp[3] = 0;
                        }
                        else if ((temp[3] - inNew.size + 11 > 0) && (temp[3] - inNew.size + 11) < (temp[1] - inNew.size + 11))
                        {
                            temp[0] = temp[2];
                            temp[1] = temp[3];
                            temp[2] = 0;
                            temp[3] = 0;
                        }
                        else
                        {
                            temp[0] = 0;
                            temp[1] = 0;
                            temp[2] = 0;
                            temp[3] = 0;
                        }
                    }
                }

                inNew.ppcb.memPos1 = temp[0];

                int j = 0;
                for (int i = temp[0]; i < temp[0] + inNew.size + 11; i++)
                {
                    if (inNew.mem[j] != -9)
                        MEM[i] = inNew.mem[j];
                    j++;
                }

                enqueue(waitQueue, inNew, waitQueueSize);
                waitQueueSize++;

            }
            else
            {
                break;
            }
        }
    }
}

void controller(int line, int blockCounter, int newCounter, FILE *fp, FILE *ss)
{
    struct program p;

    blockCounter = blockController(blockCounter);

    int indexToPrint;
    indexToPrint = searchIndex(exitProcess.ppcb.pid);
    toPrint[indexToPrint].ppcb.state = -1;
    if (exitFlag)
    {
        close();
        exitFlag = false;
    }

    if (blockFlag)
    {
        block(inRun.mem[inRun.ppcb.pc - 1]);
        blockFlag = false;
    }

    runController();

    newToWait();

    while (matrix[line][10] == clockCycle && line < 30)
    {
        p = initProcess(p);
        int count = 0;
        for (int j = 10; j < MEM_SIZE - 1; j++)
        {

            if (matrix[line][j] != -9)
            {
                p.mem[j] = matrix[line][j];
                count++;
            }
        }
        p.size = count;
        line++;
        new (p);
    }

    //printf("n: %d w: %d b: %d r: %d next: %d\n" ,newQueueSize, waitQueueSize, blockQueueSize, runCounter,nextPid);
    if (newQueueSize == 0 && waitQueueSize == 0 && blockQueueSize == 0 && runCounter == 0 && nextPid != 10) //Se nao houver nada no block/wait/new/run sai da função
    {
        print(fp,ss);
        return;
    }

    print(fp,ss);
    if (memClean < memOccupied)
        memOccupied = memClean;
    clockCycle++;
    controller(line, blockCounter, newCounter, fp, ss);
}

void readInput()
{
    char text[300];
    char instanteA;
    char instanteB;

    int i = 0;
    for (int h = 0; h < 30; h++)
    {
        for (int g = 0; g < 300; g++)
        {
            matrix[h][g] = -9;
        }
    }

    //O input é recebido por linha e guardado numa matriz para o ficheiro de input ser lido apenas uma vez
    //Recebe input referente ao instante"

    while (scanf("%c%c", &instanteA, &instanteB) != EOF)
    {
        if (instanteA >= '0' && instanteA <= '9')
        {
            if (instanteB == ' ')
            {
                int a = instanteA - '0';
                matrix[i][10] = a;
            }
            else
            {
                int a = instanteA - '0';
                int b = instanteB - '0';
                int re = (a * 10) + b;
                matrix[i][10] = re;
            }
        }

        // "recebe input com as instrucoes do processo a executar?

        if (scanf("%[^\n]%*c", text) == 1)
        {
            int j = 0;
            int k = 0;
            while (j < strlen(text))
            {
                if (text[j] >= '0' && text[j] <= '9')
                {
                    if (text[j + 1] >= '0' && text[j + 1] <= '9')
                    {
                        int b = text[j] - '0';
                        int c = text[j + 1] - '0';
                        b = (b * 10) + c;
                        matrix[i][11 + k] = b;
                        k++;
                        j += 2;
                    }
                    else
                    {
                        int b = text[j] - '0';
                        matrix[i][11 + k] = b;
                        k++;
                    }
                }
                j++;
            }
        }
        i++;
    }
}

void memInit()
{

    for (int i = 0; i < MEM_SIZE; i++)
    {
        MEM[i] = -9;
    }
}

int main()
{
    // Inicia o ficheiro para onde será escrito as variáveis

    ficheiro = fopen("file.txt", "r+");
    if (ficheiro == NULL)
    {
        ficheiro = fopen("file.txt", "w+");
    }

    FILE *fp;
    fp = fopen("sheduler_completo.out", "w+"); //cria ou reescreve o ficheiro de output
    FILE *ss;
    ss = fopen("sheduler_simples.out", "w+"); //cria ou reescreve o ficheiro de output
    memInit();
    exitProcess.ppcb.pid = -1;

    readInput();

    controller(0, 0, 0, fp,ss); //Inicia a função que irá gerir todo o escalonamento
}
